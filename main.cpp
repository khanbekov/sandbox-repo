#include <spdlog/logger.h>
#include <spdlog/sinks/stdout_sinks.h>

#include <fmt/format.h>
/**
 * Функция обратного вызова для обработки прерывания <Ctrl+C>. Приводит к остановке программы
 */

int main(int argc_, char const* argv_[]) {

    auto console = spdlog::stdout_logger_st("console");
    console->info("custom class with operator {}", 1);
    console->info(fmt::format("custom class with operator {}", 1));

    // Вернем результат работы.
    return EXIT_SUCCESS;
};

